#
# Be sure to run `pod lib lint AModule.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AModule'
  s.version          = '0.1.3'
  s.summary          = 'AModule'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
App modules
                       DESC

  s.homepage         = 'http://git.oschina.net/rogerabyss/AModule'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'RogerAbyss' => 'roger_ren@qq.com' }
  s.source           = { :git => 'https://git.oschina.net/rogerabyss/AModule.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.subspec 'Cache' do |cache|
    cache.source_files = 'AModule/Classes/Cache/*'
    cache.public_header_files = 'AModule/Classes/Cache/*.h'

    cache.dependency 'AExtension/CommonCrypto'
    cache.libraries = 'sqlite3'
  end

  s.subspec 'Secur' do |se|
    se.source_files = 'AModule/Classes/Secur/*'
    se.public_header_files = 'AModule/Classes/Secur/*.h'

    se.dependency 'AExtension/CommonCrypto'
  end

  s.subspec 'Pop' do |pop|
    pop.source_files = 'AModule/Classes/Pop/**/*'
    pop.public_header_files = 'AModule/Classes/Pop/**/*.h'

    pop.dependency 'AExtension/Core'
    pop.dependency 'AModule/Line'
    pop.dependency 'AModule/Button'
  end

  s.subspec 'Line' do |li|
    li.source_files = 'AModule/Classes/Line/**/*'
    li.public_header_files = 'AModule/Classes/Line/**/*.h'

    li.dependency 'AExtension/Core'
  end

  s.subspec 'Button' do |b|
    b.source_files = 'AModule/Classes/Button/*'
    b.public_header_files = 'AModule/Classes/Button/*.h'

    b.dependency 'AExtension/Core'
  end

  s.default_subspec = 'Cache', 'Secur', 'Pop', 'Line', 'Button'
end
