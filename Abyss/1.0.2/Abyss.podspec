#
# Be sure to run `pod lib lint Abyss.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'Abyss'
s.version          = '1.0.2'
s.summary          = 'A Friendly Fast Develop Framework Personally'


s.description      =
<<-DESC
A Friendly Fast Develop Framework Personally

[CRBase]
- CRRouter
DESC

s.homepage         = 'http://git.oschina.net/abyssroger'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'abyssroger' => 'roger_ren@qq.com' }
s.source           = { :git => 'https://git.oschina.net/abyssroger/abyss.git', :tag => s.version.to_s }

s.ios.deployment_target = '8.0'

s.frameworks = 'UIKit','Foundation'

s.source_files = ['Abyss/Classes/**/*','Abyss/Abyss.h','Abyss/Classes/*']
s.public_header_files = ['Abyss/Classes/**/*.h','Abyss/Abyss.h','Abyss/Classes/*.h']
s.resource = ['Abyss/com.abyss.bundle.bundle','Abyss/**/MJRefresh.bundle']

core_files = ['Abyss/Classes/Core/*','Abyss/Classes/Core/**/*']

s.subspec 'Core' do |core|

core.source_files = core_files

core.libraries = 'z','sqlite3'

core.dependency 'CocoaLumberjack', '~> 3.0.0'
core.dependency 'SAMKeychain', '~> 1.5.2'
core.dependency 'FLEX', '~> 2.4.0'
core.dependency 'GDPerformanceView', '~> 1.3.0'
core.dependency 'AsyncDisplayKit'
end

router_files = ['Abyss/Classes/Rounter/*','Abyss/Classes/ThirdPart/JSONKit/*']

s.subspec 'Rounter' do |router|
router.source_files = router_files
router.requires_arc  = false
router.requires_arc = 'Abyss/Classes/Rounter/*'

router.dependency 'Abyss/Core'
end

net_files = ['Abyss/Classes/Net/**/*']

s.subspec 'Net' do |net|
net.source_files = net_files

net.dependency 'AFNetworking', '~> 3.1.0'

net.dependency 'Abyss/Core'
end

ui_files = ['Abyss/Classes/UI/**/*','Abyss/Classes/ThirdPart/MJRefresh/**/*']

s.subspec 'UI' do |ui|
ui.source_files = ui_files

ui.dependency 'Abyss/Core'
ui.dependency 'Abyss/Net'
end

fonticon_files = ['Abyss/Classes/FontIcon/**/*']

s.subspec 'FontIcon' do |fi|
fi.source_files = fonticon_files
fi.dependency 'Abyss/Core'
end

s.exclude_files = ['Abyss/Classes/**/*','Abyss/Classes/*']
end
