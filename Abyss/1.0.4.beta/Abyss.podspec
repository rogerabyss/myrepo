#
# Be sure to run `pod lib lint Abyss.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'Abyss'
s.version          = '1.0.4.beta'
s.summary          = 'A Friendly Fast Develop Framework Personally'


s.description      =
<<-DESC
A Friendly Fast Develop Framework Personally

[CRBase]
- CRRouter
DESC

s.homepage         = 'http://git.oschina.net/abyssroger'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'abyssroger' => 'roger_ren@qq.com' }
s.source           = { :git => 'https://git.oschina.net/abyssroger/abyss.git', :tag => s.version.to_s }

s.ios.deployment_target = '8.0'

s.frameworks = 'UIKit','Foundation'

# Core 
core_files = ['Abyss/Classes/Core/*','Abyss/Classes/Core/**/*','Abyss/Private/Core/**/*','Abyss/com.abyss.bundle',
			  'Abyss/Classes/Net/**/*','Abyss/Private/Net/*',
			  'Abyss/Classes/UI/DataLoader/*','Abyss/Classes/UI/Banner/*','Abyss/Classes/ThirdPart/MJRefresh/**/*',
			  'Abyss/Classes/Rounter/*',
			]

s.subspec 'Core' do |core|

core.source_files = core_files
core.private_header_files = 'Abyss/Private/Core/*.h','Abyss/Private/Core/**/*.h','Abyss/Private/Net/*.h',
core.prefix_header_contents = '#import "CRPrivate.h"'

core.libraries = 'z','sqlite3'

core.dependency 'CocoaLumberjack', '~> 3.0.0'
core.dependency 'SAMKeychain', '~> 1.5.2'
core.dependency 'FLEX', '~> 2.4.0'
core.dependency 'GDPerformanceView', '~> 1.3.0'
core.dependency 'PINRemoteImage/Core', '~> 3.0.0-beta.8'

core.dependency 'AFNetworking', '~> 3.1.0'

core.resources = 'Abyss/com.abyss.bundle'

core.dependency 'Abyss/NOARC'

end

no_arc = ['Abyss/Classes/ThirdPart/JSONKit/*']

s.subspec 'NOARC' do |c|
c.source_files	 = no_arc
c.requires_arc 	 = false

end

# TODO Later
# # Router
# router_files = ['Abyss/Classes/Rounter/*','Abyss/Classes/ThirdPart/JSONKit/*']

# s.subspec 'Rounter' do |router|
# router.source_files = router_files
# router.requires_arc  = false
# router.requires_arc = 'Abyss/Classes/Rounter/*'

# router.dependency 'Abyss/Core'
# end

# # Net
# net_files = ['Abyss/Classes/Net/**/*','Abyss/Private/Net/*']

# s.subspec 'Net' do |net|
# net.source_files = net_files
# net.private_header_files = 'Abyss/Private/Net/*.h'

# net.dependency 'AFNetworking', '~> 3.1.0'

# net.dependency 'Abyss/Core'
# end

# # UI
# ui_files = ['Abyss/Classes/UI/DataLoader/*','Abyss/Classes/UI/Banner/*','Abyss/Classes/ThirdPart/MJRefresh/**/*']

# s.subspec 'UI' do |ui|
# ui.source_files = ui_files

# ui.dependency 'Abyss/Core'
# ui.dependency 'Abyss/Net'

# end

end
