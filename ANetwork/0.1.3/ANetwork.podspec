#
# Be sure to run `pod lib lint ANetwork.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ANetwork'
  s.version          = '0.1.3'
  s.summary          = 'ANetwork'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  Network Engine
                       DESC

  s.homepage         = 'http://git.oschina.net/abyssroger/ANetwork'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'RogerAbyss' => 'roger_ren@qq.com' }
  s.source           = { :git => 'https://git.oschina.net/rogerabyss/ANetWork.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  # s.source_files = 'ANetwork/Classes/**/*.{h.m}','ANetwork/Classes/*'
  s.frameworks = 'UIKit', 'Foundation'
  

  s.subspec 'Core' do |core|
    core.source_files = 'ANetwork/Classes/Core/**/*','ANetwork/Classes/Private/*','ANetwork/Classes/ANetwork.{h,m}'
    core.public_header_files = 'ANetwork/Classes/Core/**/*.h','ANetwork/Classes/Private/*.h','ANetwork/Classes/ANetwork.h'
    core.dependency 'AFNetworking'
    core.dependency 'AExtension/Config'
    core.dependency 'AExtension/Wrapper'
  end

  s.subspec 'DataLoader' do |d|
    d.source_files = 'ANetwork/Classes/DataLoader/*',
    d.public_header_files = 'ANetwork/Classes/DataLoader/*.h'
    d.dependency 'MJRefresh'
    d.dependency 'ANetwork/Core'
  end

  s.default_subspec = 'Core','DataLoader'
end
