#
# Be sure to run `pod lib lint ALauncher.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'ALauncher'
  s.version          = '0.1.5'
  s.summary          = 'ALauncher'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  Launch app
                       DESC

  s.homepage         = 'https://git.oschina.net/rogerabyss/ALauncher'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'RogerAbyss' => 'roger_ren@qq.com' }
  s.source           = { :git => 'https://git.oschina.net/rogerabyss/ALauncher.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  s.framework = 'Foundation', 'UIKit'
  s.dependency 'AExtension', '~> 0.2.1'

  s.subspec 'Core' do |core|
    core.source_files = 'ALauncher/Classes/Core/*'
    core.public_header_files = 'ALauncher/Classes/Core/*.h'
  end

  s.subspec 'Home' do |home|
    home.source_files = 'ALauncher/Classes/Home/*'
    home.public_header_files = 'ALauncher/Classes/Home/*.h'

    home.dependency 'ALauncher/Core'
    home.dependency 'ALauncher/Button'
  end

  s.subspec 'Button' do |b|
    b.source_files = 'ALauncher/Classes/Button/*'
    b.public_header_files = 'ALauncher/Classes/Button/*.h'
  end

  s.default_subspec = 'Core','Button','Home'
   
end
